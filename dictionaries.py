def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    m = s.replace(', ',' ').replace('. ',' ').replace('.',' ').split()
    k = list(set(m))
    d  = {}
    for i in k:
        d[i] = m.count(i)
    print(d)
    return d



def dict_items(d):
    c = []
    for i in d:
     c = c + [(i,d[i])]
    return c

def dict_items_sorted(d):
    s = list(sorted(d))
    c = []
    for i in s:
     c = c + [(i,d[i])]
    return c
