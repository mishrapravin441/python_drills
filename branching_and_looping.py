def integers_from_start_to_end_using_range(start, end, step):
    m = []
    for i in range(start,end,step):
        m = m + [i]
    """return a list"""
    return m


def integers_from_start_to_end_using_while(start, end, step):
    """return a list"""
    m = []
    b = abs((abs(start)-abs(end))/step)
    if b != int(b) :
        b = b + 1
    a = 1
    print(a,b)
    while a<=b:
        m = m + [start]
        start = start + step
        a = a + 1
    print(m)
    return m


def is_prime_number(x):
    if x >= 2:
        for y in range(2, x):
            if not (x % y):
                return False
    else:
        return False
    return True


def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    m = []
    for i in range(10,100):
        if is_prime_number(i):
            m = m + [i]
    return m


