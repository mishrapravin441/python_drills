def last_3_characters(x):
    if len(x)>=3:
     return x[len(x)-3:]
    else :
        return x


def first_10_characters(x):
    return x[:10]


def chars_4_through_10(x):
    if len(x)>4:
     return x[4:11]
    else :
        return ''


def str_length(x):
    return len(x)


def words(x):
    return x.split()


def capitalize(x):
    return x.capitalize()


def to_uppercase(x):
    return x.upper()
