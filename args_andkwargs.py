def add3(a, b, c):
    return sum([a, b, c])


def unpacking1():
    items = [1, 2, 3]
    add3(*items)


def unpacking2():
    kwargs = {'a': 1, 'b': 2, 'c': 3}
    add3(**kwargs)


def call_function_using_keyword_arguments_example():
    a = 1
    b = 2
    c = 3
    add3(a=1,b=2,c=3)


def add_n(*args):
    return sum(args)


def add_kwargs(**kwargs):
    k = 0
    for i in kwargs:
        if i != "a" and i != "b":
            return "Problem"
        else:
            k = k + kwargs[i]
    return k


def universal_acceptor(*args,**kwargs):
    return args,kwargs