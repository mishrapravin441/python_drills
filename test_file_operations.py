import unittest

from file_operations import *


class TestFileOperations(unittest.TestCase):
    def test_operations(self):
        write_to_file('./data.txt', 'Simple is better than complex.\n')
        s = read_file('./data.txt')
        self.assertEqual(s, 'Simple is better than complex.\n')

        append_to_file('./data.txt', 'Explicit is better than implicit.\n')
        s2 = read_file('./data.txt')
        self.assertEqual(s2, 'Simple is better than complex.\nExplicit is better than implicit.\n')
if __name__ == '__main__':
    unittest.main()
