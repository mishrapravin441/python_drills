solution = []
def html_dict_search(html_dict, selector):
    global solution
    classId = selector[1:]
    if not html_dict:
        return solution
    try:
        if bool(html_dict['attrs']) and html_dict['attrs']['class']==classId:
            solution = solution + html_dict
        if bool(html_dict['children']):
            for child in html_dict['children']:
                html_dict_search(child,selector)
    except KeyError as e:
        if html_dict['attrs']['id']==classId:
            solution = solution + html_dict
    return solution
