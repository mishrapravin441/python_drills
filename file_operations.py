"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    f =  open(path, 'r')
    data = f.read()
    f.close()
    return data


def write_to_file(path, s):
    f = open(path,'w+')
    f.write(s)
    f.close()
    return


def append_to_file(path, s):
    f =  open(path, 'a')
    data = f.write(s)
    f.close()
    return data

def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    pass
